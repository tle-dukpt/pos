module tle/pos

go 1.18

require (
	github.com/albenik/bcd v0.0.0-20170831201648-635201416bc7 // indirect
	github.com/juju/errors v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
