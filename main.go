// Program: POS Terminal Line Encryption with DUKPT
// Email: husnifahmi@outlook.com
// Date: 2022-12-30
// 3 DUKPT Key Management Scheme
// Derived Unique Key Per Transaction (DUKPT) is a key management scheme
// in which for every transaction, a unique key is used which is derived
// from a fixed key. Therefore, if a derived key is compromised, future
// and past transaction data are still protected since the next or prior
// keys cannot be determined easily. DUKPT is specified in ANSI X9.24 part 1
//
//
//
// 3.1 In Brief
// • The transaction request from POS terminal to payment processing gateway needs to be encrypted.
// • Of course! How is the encryption done?
// 		By using the well known Triple DES crypto algorithm. And each transaction will have unique
// 		key.
// • Unique? But how are the keys to algorithm known? Are they loaded in the terminal and gateway
// beforehand?
// 		No. Key is generated dynamically with each transaction. The terminal while doing the current
// 		transaction will generate the next one and store it. The key for the current transaction is
// 		cryptographically changed to the key for the next transaction.
// • Ok. But how does the gateway know about it and how does it decrypt?
// 		Gateway would know because, it has the mother of all keys. The Super-secret key. When the
// 		client sends the encrypted payload it also sends some meta-data (unencrypted). With the metadata and the Super-secret key the gateway would cryptographically generate the same key that
// 		terminal used for the encryption.
// • Great! What is this mechanism called?
// 		DUKPT - Derived Unique Key Per Transaction
//

// https://stackoverflow.com/questions/48124565/why-does-vscode-delete-golang-source-on-save
// https://www.developer.com/languages/intro-socket-programming-go/

package main
import ( 
	"crypto/cipher"
	"crypto/des"
	"encoding/binary"
	"bytes"
	"os"
	"fmt"
	"net"
	"tle/pos/dukpt"
)
const (
	SERVER_HOST = "localhost"
	SERVER_PORT = "9988"
	SERVER_TYPE = "tcp"
)

func main() {
	initIpek()
		
	// Base Derivation Key (BDK), Super-secret key, stored in HSM
	var testBdk = []byte{0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 
						 0xFE, 0xDC, 0xBA, 0x98, 0x76, 0x54, 0x32, 0x10}

	// Key Serial Number (Key Name): Key-Set ID, TRSM ID, Transaction Counter; 10 bytes
	// The Key Name shall be 80 bits – 10 Bytes
	// 		Transaction Counter – 21 Bits 
	// 		TRSM ID – 19 Bits 
	// 		Key Set ID – 40 bits (5 bytes) 
	var testKsn = []byte{0xFF, 0xFF, 0x98, 0x76, 0x54, 0x32, 0x10, 0xE0, 0x00, 0x08}

	// On the terminal side: 
	// Once the terminal has been initialized with IPEK, it shall populate the 
	// 21 Future Key registers by invoking a non-reversible transformation process.
	// The inputs to this would be IPEK and a value which is function of the 
	// register number. Then the IPEK is discarded. Now the terminal has 21 
	// Future Keys stored in 21 registers. These 21 keys shall be mother of
	// all future keys. The non-reversible transformation process shall involve
	// 3DES in the process. 
	// Now for the first transaction request that needs to be sent to the TG,
	// the key corresponding to counter value = 1 is located in the first register.
	// Once the transaction is completed the content of this register is erased
	// in order to preserve forward-secrecy. 
	var testIpek = []byte{0x6a, 0xc2, 0x92, 0xfa, 0xa1, 0x31, 0x5b, 0x4d,
						  0x85, 0x8a, 0xb3, 0xa3, 0xd7, 0xd5, 0x93, 0x3a}

	// For the next transaction request, the counter value gets incremented to 2
	// and the key from second register is chosen. Once the transaction is 
	// completed the content of this register is erased but before that the key
	// is input to the non-reversible transformation process along with the 
	// current counter value. The result is a new Future key which gets stored
	// in register 1. This process shall continue for each transaction. Now it
	// is important to note that the X9.24 specifies some optimizations in the
	// key derivation process and might vary from what is described here but
	// still the essence is the same.

	// Counter value gets incremented, Future key chosen, encryption done,
	// before discarding invoke the non-reversible transformation to create
	// a new key and store.

	// Now the Key-Name comprising the Key Set ID, TRSM ID and the Counter is
	// the meta-data that was discussed earlier. 
	// The Key-Name should be sent along with the encrypted transaction data.
	// More precisely the meta data should precede the encrypted ISO-8583 frame.
	// The Key-Name will be not encrypted.

	// 1. Generate BDK. Store it in HSM from which TG can access. 
	// 2. From BDK generate IPEK. Inject IPEK into the terminal in a secure
	//	  and controlled manner.
	// 3. Input the Key-Set ID and TRSM ID (which were used for generating the BDK) 
	// 4. Invoke DUKPT software on the terminal. This should result in Future Key
	// 	  getting generated and stored. 
	// 5. Terminal shall use only TDES for the encryption. 
	// 6. For every transaction that the terminal shall send to TG, it shall send
	// along with the encrypted ISO-8583 frame/EIS frame 10 bytes of Meta Data.

	// 2. Derive IPEK from BDK and KSN
	// Once the BDK is identified, we need to generate what is known as 
	// Initial PIN Encryption Key (IPEK). Another set of inputs for generating
	// IPEK are Key-Set ID and TRSM ID. Key-Set ID uniquely identifies the BDK.
	// TRSM ID is the device ID of the transaction originating terminals. 
	ipek, err := dukpt.DeriveIpekFromBdk(testBdk, testKsn)
	_ = err
	idx := 0
	fmt.Printf("ipek    : ")
	for idx = 0; idx < 16; idx++ {
		fmt.Printf("%02X ", ipek[idx])
	}
	fmt.Println()

	fmt.Printf("testIpek: ")
	for idx = 0; idx < 16; idx++ {
		// fmt.Printf("%02X ", testIpek[idx] >> 8)
		fmt.Printf("%02X ", testIpek[idx])
	}
	fmt.Println()

	sameIpek := 1
	for idx = 0; idx < 16; idx++ {
		if ipek[idx] != testIpek[idx] {
			fmt.Printf("Error: ipek[%d] %04X != %04X testIpek[%d]\n", 
							idx, ipek[idx], idx, testIpek[idx])
			sameIpek = 0
		}
	}

	if ( sameIpek == 1 ) {
		fmt.Printf("ipek is equal to testIpek\n")
	} else {
		fmt.Printf("ipek is NOT equal to testIpek\n")
		os.Exit(1)
	}
	fmt.Println()

	// Derive First Key (PEK) 
	pek, err := dukpt.DerivePekFromIpek(ipek, testKsn)
	fmt.Printf("Derived key PEK: ")
	for idx = 0; idx < 16; idx++ {
		fmt.Printf("%02X ", pek[idx])
	}
	fmt.Println()
	_ = err

	var testPek = []byte{0x27, 0xf6, 0x6d, 0x52, 0x44, 0xff, 0x62, 0x1e, 0xaa, 0x6f, 0x61, 0x20, 0xed, 0xeb, 0x42, 0x7f}
	fmt.Printf("testPEK        : ")
	for idx = 0; idx < 16; idx++ {
		fmt.Printf("%02X ", testPek[idx])
	}
	fmt.Println()

	res := bytes.Compare(pek, testPek)  
    if res == 0 {
        fmt.Println("pek is equal to testPek")
    } else {
        fmt.Println("pek is NOT equal to testPek")
    }

	// 5. Terminal shall use only TDES for the encryption. 
	// 6. For every transaction that the terminal shall send to TG, it shall send
	// along with the encrypted ISO-8583 frame/EIS frame 10 bytes of Meta Data.
	// buffer := make([]byte, 1024)
	// []byte("Hello Server! Greetings."))
	// buffer := make([]byte, 1024)
	var plaintext = "Hello Server! Greetings. Good morning"
	bPlaintext := []byte(plaintext)

	// encrypt bpaddedPlaintext using TDES with key length 128 bits
	bpaddedCiphertext := encryptTDES(bPlaintext, pek)

	// 4 Use Case for TLE with DUKPT 
	// 4.1 TPDU Enabled ISO 8583
	
	// Let us consider the case of securing the TPDU enabled ISO 8583
	// transaction from POS terminal to the TG. 
	// TPDU header carries the host routing information.
	// The request packet format is as shown below.

	// 1. Message overall length: 2 bytes (Big Endian); offset: byte 0
	// 2. Unencrypted TPDU header for host routing: 5 bytes; offset: byte 2
	// 3. Information to indicate encryption - OxEE: 1 byte; offset: byte 7
	// 4. Key-Set ID; offset: byte 8
	// 5. TRSM ID
	// 6. 21 bits comprising the transaction counter
	// The Key Name shall be 80 bits – 10 Bytes
	// 		Transaction Counter – 21 Bits 
	// 		TRSM ID – 19 Bits 
	// 		Key Set ID – 40 bits (5 bytes) 
	// Length of point 4, 5, and 6 combined: 10 bytes
	// 7. Length of the unencrypted ISO-8583 frame: 2 bytes; offset: byte 18
	// 8. TDES encrypted ISO-8583 frame: variable; offset: byte 20

	// Generate TPDU
	tpduMsg := generateTPDU(testKsn, bpaddedCiphertext)

	// Parse TPDU
	parseTPDU(tpduMsg)

	os.Exit(0)

	// establish connection
	connection, err := net.Dial(SERVER_TYPE, SERVER_HOST+":"+SERVER_PORT)
	if err != nil {
		panic(err)
	}

	///send some data
	_, err = connection.Write([]byte("Hello Server! Greetings."))
	_ = err
	buffer := make([]byte, 1024)
	mLen, err := connection.Read(buffer)
	if err != nil {
		fmt.Println("Error reading:", err.Error())
	}
	fmt.Println("Received: ", string(buffer[:mLen]))
	defer connection.Close()
	
	_ = err

}

func buildTdesKey(key []byte) []byte {
	var finalKey []byte

	if len(key) == 24 {
		finalKey = key
	} else if len(key) == 16 {
		finalKey = make([]byte, 24)
		copy(finalKey, key)
		copy(finalKey[16:], key[:8])
	}

	return finalKey
}

// https://www.cryptosys.net/pki/manpki/pki_paddingschemes.html
// Padding schemes for block ciphers
// To perform encryption with a block cipher in ECB or CBC mode
// the length of the input to be encrypted must be an exact
// multiple of the block length B in bytes. For Triple DES
// the block length B is 8 bytes (64 bits) and 
// for all AES variants it is 16 bytes (128 bits). If the length
// of the data to be encrypted is not an exact multiple of B,
// it must be padded to make it so. After decrypting,
// the padding needs to be removed.

// There are many, many conventions for padding. It is up to
// the sender and receiver of encrypted data to agree on the
// convention used.
// The most popular is "PKCS5" padding, described in section 
// 6.1.1 of [PKCS5], which is the same as the padding method
// in section 6.3 of [CMS], section 10.3 of [PKCS7] and 
// para 1.1 of [RFC1423].
// PKCS5 Padding
// If the block length is B then add N padding bytes of value N
// to make the input length up to the next exact multiple of B.
// If the input length is already an exact multiple of B then
// add B bytes of value B. Thus padding of length N between one
// and B bytes is always added in an unambiguous manner. After 
// decrypting, check that the last N bytes of the decrypted data
// all have value N with 1 < N ≤ B. If so, strip N bytes, 
// otherwise throw a decryption error.
// Examples of PKCS5 padding for block length B = 8:
// 3 bytes: FDFDFD           --> FDFDFD0505050505
// 7 bytes: FDFDFDFDFDFDFD   --> FDFDFDFDFDFDFD01
// 8 bytes: FDFDFDFDFDFDFDFD --> FDFDFDFDFDFDFDFD0808080808080808
// https://www.cryptosys.net/pki/manpki/pki_paddingschemes.html

func padPlaintext(bplaintext []byte) []byte {
	fmt.Println()
	fmt.Println("Function: padPlaintext()")

	bMsgLen := len(bplaintext)
	modLen := bMsgLen % 8
	padByte := byte (8 - modLen)
	numBytes := int (padByte)
	if modLen != 0 {
		paddedLen := bMsgLen + numBytes
		fmt.Printf("bmsgLen\t\t: %d\n", bMsgLen)
		fmt.Printf("padByte\t\t: %d\n", padByte)
		fmt.Printf("paddedLen\t: %d\n", paddedLen)
		idx := paddedLen - numBytes
		bpaddedPlaintext := make([]byte, paddedLen)
		copy(bpaddedPlaintext, bplaintext)
		for i:=0; i<numBytes; i++ {
			bpaddedPlaintext[idx+i] = padByte
		}
		return bpaddedPlaintext
	}

	if modLen == 0 {
		// add 8 bytes of 0x08
		paddedLen := bMsgLen + 8
		fmt.Printf("bmsgLen\t\t: %d\n", bMsgLen)
		fmt.Printf("padByte\t\t: %d\n", padByte)
		fmt.Printf("paddedLen\t: %d\n", paddedLen)
		bpaddedPlaintext := make([]byte, paddedLen)
		copy(bpaddedPlaintext, bplaintext)
		j := 0
		for i:=0; i<8; i++ {
			j = bMsgLen + i
			bpaddedPlaintext[j] = 0x08
		}

		// fmt.Println("bpaddedPlaintext: ", bpaddedPlaintext)
		return bpaddedPlaintext
	}
	
	return nil
}

func removePadBlock(bplaintext []byte) []byte {
	fmt.Println()
	fmt.Println("Function: removePadBlock()")

	bLen := len(bplaintext)
	modLen := bLen % 8
	if modLen != 0 {
		return []byte("error")
	}

	padByte := int (bplaintext[bLen - 1])
	newLen := bLen - padByte
	newPlaintext := make([]byte, newLen)
	copy(newPlaintext, bplaintext[:newLen])

	return newPlaintext
}

func parseTPDU(tpduMsg []byte) {
	fmt.Println()
	fmt.Println("Function: parseTPDU()")

	// 1. Message overall length: 2 bytes (Big Endian); offset: byte 0
	// 2. Unencrypted TPDU header for host routing: 5 bytes; offset: byte 2
	// 3. Information to indicate encryption - OxEE: 1 byte; offset: byte 7
	// 4. Key-Set ID; offset: byte 8
	// 5. TRSM ID
	// 6. 21 bits comprising the transaction counter
	// Length of point 4, 5, and 6 combined: 10 bytes
	// 7. Length of the unencrypted ISO-8583 frame: 2 bytes; offset: byte 18
	// 8. TDES encrypted ISO-8583 frame: variable; offset: byte 20

	blength := len(tpduMsg)
	if ( blength == 0 ) {
		fmt.Println("Message length cannot be zero.")
		return
	}

	// 1. Message overall length: 2 bytes (Big Endian); offset: byte 0
	msgIdx := 0
	msgLen := binary.BigEndian.Uint16(tpduMsg[0:2])
	msgIdx += 2
	fmt.Printf("Message overall length: %d\n", msgLen)

	// 2. Unencrypted TPDU header for host routing: 5 bytes; offset: byte 2
	tpduHeader := tpduMsg[msgIdx:msgIdx+5]
	msgIdx += 5
	fmt.Println("TPDU Header for host routing: ", tpduHeader)

	// 3. Information to indicate encryption - OxEE: 1 byte; offset: byte 7
	encIndicator := tpduMsg[msgIdx]
	msgIdx += 1
	fmt.Printf("Encryption indicator: %02X\n", encIndicator)

	// 4. Key-Set ID; offset: byte 8
	// 5. TRSM ID
	// 6. 21 bits comprising the transaction counter
	// Length of point 4, 5, and 6 combined: 10 bytes
	// 		Transaction Counter – 21 Bits 
	// 		TRSM ID – 19 Bits 
	// 		Key Set ID – 40 bits (5 bytes)
	keyKsn := tpduMsg[msgIdx:msgIdx+10]
	keySetID := tpduMsg[msgIdx:msgIdx+5]
	msgIdx += 5
	fmt.Println("Key-Set ID: ", keySetID)

	trsmID := tpduMsg[msgIdx:msgIdx+3]
	msgIdx += 2
	trsmID[2] = trsmID[2] & 0xE0
	fmt.Println("TRSM ID: ", trsmID)

	bcounter := make([]byte, 4)
	copy(bcounter[1:], tpduMsg[msgIdx:msgIdx+3])
	fmt.Println("tpduMsg[msgIdx:msgIdx+3]: ", tpduMsg[msgIdx:msgIdx+3])
	msgIdx += 3
	bcounter[1] = bcounter[1] & 0X1F
	counter := binary.BigEndian.Uint32(bcounter)
	fmt.Println("bcounter: ", bcounter)
	fmt.Println("counter: ", counter)

	// 7. Length of the unencrypted ISO-8583 frame: 2 bytes; offset: byte 18
	bpaddedLen := tpduMsg[msgIdx:msgIdx+2]
	msgIdx += 2
	paddedLen := binary.BigEndian.Uint16(bpaddedLen)
	fmt.Println("Unecnrypted ISO-8583 frame with padding: ", paddedLen)

	// 8. TDES encrypted ISO-8583 frame: variable; offset: byte 20
	encryptedISO8583 := tpduMsg[msgIdx:msgIdx+(int)(paddedLen)]

	// retrieve Base Derivation Key using Key-Set ID and TRSM ID
	// keyBdk := retrieveBDK(keySetID, trsmID)
	keyPek := deriveTDESKey(keyKsn)
	decryptedISO8583 := decryptTDES(encryptedISO8583, keyPek)

	fmt.Println()
	fmt.Println("Unencrypted ISO-8583 frame: ", decryptedISO8583)
	fmt.Printf("Unencrypted ISO-8583 frame: %s\n", decryptedISO8583)
}

func retrieveBDK(keySetID []byte, trsmID []byte) []byte {
	fmt.Println()
	fmt.Println("Function: retrieveBDK()")

	var testBdk = []byte{0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 
		0xFE, 0xDC, 0xBA, 0x98, 0x76, 0x54, 0x32, 0x10}
	var testKsn = []byte{0xFF, 0xFF, 0x98, 0x76, 0x54, 0x32, 0x10, 0xE0, 0x00, 0x08}
	ksn := dukpt.DecodeKsn(testKsn)

	resK := bytes.Compare(keySetID, ksn.Ksi)  
    if resK == 0 {
        fmt.Println("keySetIID is equal to Ksn.Ksi")
    } else {
        fmt.Println("keySetIID is NOT equal to Ksn.Ksi")
    }

	resT := bytes.Compare(trsmID, ksn.Trsm)  
    if resT == 0 {
        fmt.Println("trsmID is equal to Ksn.Trsm")
    } else {
        fmt.Println("trsmID is equal to Ksn.Trsm")
    }

	if ( resK == 0 && resT == 0 ) {
		fmt.Println("return testBdk: ", testBdk)
		return testBdk
	} else {
		zeroBdk := make([]byte, 16)
		return zeroBdk
	}
}

func deriveTDESKey(keyKsn []byte) []byte {
	fmt.Println()
	fmt.Println("Function: derviveTDESKey()")

	decKsn := dukpt.DecodeKsn(keyKsn)
	keyBdk := retrieveBDK(decKsn.Ksi, decKsn.Trsm)

	keyPek, err := dukpt.DerivePekFromBdk(keyBdk, keyKsn)
	_ = err

	return keyPek
}

func decryptTDES(ciphertext []byte, keyPek []byte) []byte {
	fmt.Println()
	fmt.Println("Function: decryptTDES()")

	iv := make([]byte, 8)
	block, err := des.NewTripleDESCipher(buildTdesKey(keyPek))
	_ = err

	// decrypt bpaddedCiphertext into bpaddedPlaintext
	decrypter := cipher.NewCBCDecrypter(block, iv)
	decLen := len(ciphertext)
	bpaddedDecrypted := make([]byte, decLen)
	decrypter.CryptBlocks(bpaddedDecrypted, ciphertext)
	fmt.Printf("%x decrypt to %s\n", ciphertext, bpaddedDecrypted)
	fmt.Printf("bpaddedDecrypted: %x\n", bpaddedDecrypted)
	bDecrypted := removePadBlock(bpaddedDecrypted)
	fmt.Printf("bDecrypted: %d\n", bDecrypted)
	fmt.Printf("bDecrypted: %s\n", bDecrypted)

	return bDecrypted
}

func generateTPDU(testKsn []byte, bpaddedCiphertext []byte) []byte {
	fmt.Println()
	fmt.Println("Function: generateTPDU()")

	// 1. Message overall length: 2 bytes (Big Endian); offset: byte 0
	// 2. Unencrypted TPDU header for host routing: 5 bytes; offset: byte 2
	// 3. Information to indicate encryption - OxEE: 1 byte; offset: byte 7
	// 4. Key-Set ID; offset: byte 8
	// 5. TRSM ID
	// 6. 21 bits comprising the transaction counter
	// The Key Name shall be 80 bits – 10 Bytes
	// 		Transaction Counter – 21 Bits 
	// 		TRSM ID – 19 Bits 
	// 		Key Set ID – 40 bits (5 bytes) 
	// Length of point 4, 5, and 6 combined: 10 bytes
	// 7. Length of the unencrypted ISO-8583 frame: 2 bytes; offset: byte 18
	// 8. TDES encrypted ISO-8583 frame: variable; offset: byte 20

	tpduMsg := make([]byte, 4096)

	// 1. Message overall length: 2 bytes (Big Endian); offset: byte 0
	var msgLen uint16 = 2

	// 2. Unencrypted TPDU header for host routing: 5 bytes; offset: byte 2
	// TPDU header with dummy routing
	var tpduHeader = []byte{0x57, 0x77, 0x85, 0x12, 0x34};
	copy(tpduMsg[2:7], tpduHeader)
	msgLen = msgLen + 5
	fmt.Printf("TPDU Header for host routing: %d\n", tpduMsg[2:7])

	// 3. Information to indicate encryption - OxEE: 1 byte; offset: byte 7
	tpduMsg[7] = 0xEE;
	msgLen = msgLen + 1

	// 4. Key-Set ID; offset: byte 8
	// 5. TRSM ID
	// 6. 21 bits comprising the transaction counter
	// Length of point 4, 5, and 6 combined: 10 bytes
	copy(tpduMsg[8:18], testKsn);
	msgLen = msgLen + 10

	fmt.Println("testKsn        : ", testKsn)
	fmt.Println("tpduMsg[8:18] : ", tpduMsg[8:18])

	// 7. Length of the unencrypted ISO-8583 frame: 2 bytes; offset: byte 18
	// extrapadbLen := padbLen + 8
	// epdLen := uint16 (extrapadbLen)
	bpaddedLen16 := uint16 (len(bpaddedCiphertext))
	binary.BigEndian.PutUint16(tpduMsg[18:20], bpaddedLen16)
	msgLen = msgLen + 2
	fmt.Printf("bpaddedLen16: %d, msgLen: %d\n", bpaddedLen16, msgLen)

	// 8. TDES encrypted ISO-8583 frame: variable; offset: byte 20
	copy(tpduMsg[20:], bpaddedCiphertext)
	msgLen = msgLen + bpaddedLen16
	fmt.Printf("msgLen: %d\n", msgLen)

	// 1. Message overall length: 2 bytes (Big Endian); offset: byte 0
	binary.BigEndian.PutUint16(tpduMsg[0:2], msgLen)
	// fmt.Printf("msgLen in TPDU: [%02X %02X] %d %d\n", tpduMsg[0], tpduMsg[1], 
	// 				tpduMsg[0], tpduMsg[1])
	
	return tpduMsg;
}

func encryptTDES(bPlaintext []byte, pek []byte) []byte {
	fmt.Println()
	fmt.Println("Function: encryptTDES()")

	bpaddedPlaintext := padPlaintext(bPlaintext);
	fmt.Println()
	bpaddedLen := len(bpaddedPlaintext)
	
	fmt.Printf("plaintext: %s\n", bPlaintext)
	fmt.Printf("bPlaintext: ")
	printHexa(bPlaintext)
	fmt.Println()
	fmt.Printf("bpaddedPlaintext: ")
	printHexa(bpaddedPlaintext)
	fmt.Println()
	fmt.Println("bpaddedLen: ", bpaddedLen)

	// encrypt bpaddedPlaintext using TDES with key length 128 bits
	iv := make([]byte, 8)
	block, err := des.NewTripleDESCipher(buildTdesKey(pek))
	_ = err

	mode := cipher.NewCBCEncrypter(block, iv)
	ciphertext := make([]byte, len(bpaddedPlaintext))
	mode.CryptBlocks(ciphertext, bpaddedPlaintext)
	// fmt.Printf("%s encrypt to %x \n", bpaddedPlaintext, bpaddedCiphertext)
	fmt.Printf("ciphertext: ")
	printHexa(ciphertext)
	fmt.Println()

	return ciphertext
}

func printHexa(bmsg []byte) {
	length := len(bmsg)
	for idx := 0; idx < length; idx++ {
		fmt.Printf("%02X ", bmsg[idx])
	}
}
