// Filename: initipek.go
// Email: husnifahmi@outlook.com
// Date: 2023-01-12
// 
// 3.5 Online IPEK Negotiation 
// Procedure provided in Sec 3.1.4 showed IPEK to be generated and provided offline 
// to be injected into terminal. 
// In alternate mechanisms IPEK can be negotiated online using request/response 
// message exchange. 
// NewNet proposes three ways of doing IPEK Negotiation. The three methods are: 
// 1. Secure IPEK Transfer with RSA X.509 Certificate 
// 2. Secure IPEK Transfer with RSA Transkrypt Keys 
// 3. Secure IPEK Transfer with RSA Terminal Keys 
// All the messages will adhere to ISO 8583 message format. For EIS enabled terminals, 
// the data will be enclosed within the control bytes - namely, STX, ETX and LRC. 
// Parity shall not be applicable on the payload bytes but only on the enclosing 
// control bytes - STX, ETX and LRC. 

// IPEK Request: 
//  Once the connection from the terminal to AG is made, the terminal needs to send in the IPEK-REQ 
// message. This message shall contain message type, version number, the Key-Set ID which 
// uniquely identifies the BDK stored in TransKrypt and the X.509 PEM public certificate
// of the terminal.

package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	// "net"
	"encoding/binary"
	"strconv"
	// "path/filepath"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	// log "github.com/sirupsen/logrus"
	errors "github.com/juju/errors"
	bcd "github.com/albenik/bcd"
)

func initIpek() {
	// NewNet proposes three ways of doing IPEK Negotiation. The three methods are: 
	// 1. Secure IPEK Transfer with RSA X.509 Certificate 
	// 2. Secure IPEK Transfer with RSA Transkrypt Keys 
	// 3. Secure IPEK Transfer with RSA Terminal Keys
	//
	fmt.Println("initIpek()")
	ipekRequest := composeIpekRequest()
	msgLen := binary.BigEndian.Uint16(ipekRequest[0:2])
	fmt.Printf("Total message length of IPEK Request: %d\n", msgLen)

	// const (
	// 	SERVER_HOST = "localhost"
	// 	SERVER_PORT = "9988"
	// 	SERVER_TYPE = "tcp"
	// )
	
	// // establish connection
	// connection, err := net.Dial(SERVER_TYPE, SERVER_HOST+":"+SERVER_PORT)
	// if err != nil {
	// 	panic(err)
	// }
	
	// ///send some data
	// sentLen, err := connection.Write(ipekRequest[0:msgLen])
	// fmt.Printf("sentLen: %d\n", sentLen)
	// _ = err
	// // _, err = connection.Write([]byte("Hello Server! Greetings."))
	ipekResponse := make([]byte, 4096)
	// recvLen, err := connection.Read(ipekResponse)
	// if err != nil {
	// 	fmt.Println("Error reading:", err.Error())
	// }
	// fmt.Println("Received: ", string(ipekResponse[:recvLen]))

	parseIpekResponse(ipekResponse)
	// defer connection.Close()
		
	// _ = err
	
}


func composeIpekRequest() []byte {
	// IPEK Request
	// ISO 8583 Enabled Terminal:
	// 1. Total message length: 2 bytes, offset 0
	// 2. TPDU Header: 5 bytes, offset 2
	// 3. MTI: 2 bytes, offset 7
	// 4. Bit map: 8 bytes, offset 9
	// 5. Processing code: Field 3, 3 bytes, offset 17
	// 6. System Trace Audit Number: 3 bytes, offset 20
	// 7. NII: 2 bytes, offset 23
	// 8. Terminal ID: 8 bytes, offset 25
	// 9. KEY Set ID: 10 bytes, offset 33
	// 10. Data Field: variable, offset 43

	ipekRequest := make([]byte, 4096)
	// _ = ipekRequest
	// 1. Total message length: 2 bytes, offset 0
	// 1. running message length: 2 bytes
	ipekRequest[0] = 0

	// 2. TPDU Header: 5 bytes, offset 2
	// 2. running message length: 7 bytes
	// TPDU Header 5 Byte 1 = 0x60 
	// Byte 2&3 = NII (can be 0x00 for EIS based messages) 
	// Byte 4&5 
	// Method 1: 
	// IPEK REQ [0x00,0x01] 
	// IPEK RESP [0x00,0x02] 
	// Method 2: 
	// PUB_KEY REQ [0x00,0x03] 
	// PUB_KEY RESP [0x00,0x04] 
	// IPEK REQ [0x00,0x05] 
	// IPEK RESP [0x00,0x06] 
	// Method 3: 
	// IPEK REQ [0x00,0x07] 
	// IPEK RESP [0x00,0x08] 

	// We are using Method 1:  
	// 1. Secure IPEK Transfer with RSA X.509 Certificate

	// 2. TPDU Header: 5 bytes, offset 2
	tpduOffset := 2
	// TPDU Header 5 Byte 1 = 0x60 
	ipekRequest[tpduOffset+0] = 0x60
	// Byte 2&3 = NII (can be 0x00 for EIS based messages)
	countryCode := "ID"
	bcountryCode := []byte(countryCode) 
	fmt.Println("Country Code: ")
	for i:= 0; i<2; i++ {
		fmt.Printf("%02X ", bcountryCode[i])
		fmt.Printf("%d ", bcountryCode[i])
	}
	fmt.Println()
	// sample NII using country code "ID"
	// I don't know what is the correct NII
	// ID is used as an example
	ipekRequest[tpduOffset+1] = bcountryCode[0]
	ipekRequest[tpduOffset+2] = bcountryCode[1]

	// Byte 4&5 
	// Method 1: 
	// IPEK REQ [0x00,0x01] 
	// IPEK RESP [0x00,0x02] 
	ipekRequest[tpduOffset+3] = 0x00
	ipekRequest[tpduOffset+4] = 0x01

	// 3. MTI: 2 bytes, offset 7
	// 3. running message length: 9 bytes
	// [0x06, 0x00] Fixed for all IPEK initialization messages 
	mtiOffset := 7
	ipekRequest[mtiOffset+0] = 0x06
	ipekRequest[mtiOffset+1] = 0x00

	// 4. Bit map: 8 bytes, offset 9
	// 4. running message length: 17 bytes
	// Bits will be set for fields present in the ISO 8583 message
	bitmapOffset := 9
	_ = bitmapOffset
	// 1 2 ...								32
	// 0010 0000 0010 0000 0000 0001 0000 0000
	// 33 34 ...							64
	// 0000 0000 1000 0000 0000 0000 0000 1110
	// hexadecimal: 0x20 0x20 0x01 0x00
	// hexadecimal: 0x00 0x80 0x00 0x08
	ipekRequest[bitmapOffset+0] = 0x20
	ipekRequest[bitmapOffset+1] = 0x20
	ipekRequest[bitmapOffset+2] = 0x01
	ipekRequest[bitmapOffset+3] = 0x00
	ipekRequest[bitmapOffset+4] = 0x00
	ipekRequest[bitmapOffset+5] = 0x80
	ipekRequest[bitmapOffset+6] = 0x00
	ipekRequest[bitmapOffset+7] = 0xE0

	// Bit map: Field 3, Field 11, Field 24, Field 41
	// Bit map: Field 61, Field 62, Field 63
	// the length of public certificate data is greater than 999
	// Field 62 is set, Field 63 is set
	// Processing Code 3, 3rd field in ISO 8583 message 
	// Following values are encoded for each method in request and 
	// response 
	// Method 1 = [0x31,0x00,0x00] 
	// Method 2 = [0x32,0x00,0x00] 
	// Method 3 = [0x33,0x00,0x00]

	// 5. Processing code: Field 3, 3 bytes, offset 17
	// 5. running message length: 20 bytes
	procOffset := 17
	// Method 1 = [0x31,0x00,0x00] 
	ipekRequest[procOffset+0] = 0x31
	ipekRequest[procOffset+1] = 0x00
	ipekRequest[procOffset+2] = 0x00

	// 6. System Trace Audit Number: 3 bytes, offset 20
	// 6. running message length: 23 bytes
	// 11th field in ISO 8583 message Transkrypt will ignore this.
	// Will echo the same value in responses
	// An ARN is usually longer than a STAN, making it a more unique number.
	// While a STAN is a six-digit code that rolls over after 
	// reaching 999,999, ARNs take longer to reach their roll-over limit.
	// https://www.heartland.us/resources/blog/what-is-a-system-trace-audit-number-stan#:~:text=A%20STAN%20is%20a%20unique,and%20Mastercard%20credit%20card%20purchases.
	auditOffset := 20
	var stanNumber uint32 = 257
	bstanNumber := make([]byte, 4)
	binary.BigEndian.PutUint32(bstanNumber, stanNumber)
	ipekRequest[auditOffset+0] = bstanNumber[1]
	ipekRequest[auditOffset+1] = bstanNumber[2]
	ipekRequest[auditOffset+2] = bstanNumber[3]
	fmt.Printf("STAN: %02X %02X %02X\n", bstanNumber[1], bstanNumber[2], bstanNumber[3])
	
	// 7. NII: 2 bytes, offset 23
	// 7. running message length: 25 bytes
	// 24th field in ISO 8583 message Transkrypt will ignore this.
	// Will echo the same value in responses 
	niiOffset := 23
	ipekRequest[niiOffset+1] = bcountryCode[0]
	ipekRequest[niiOffset+2] = bcountryCode[1]
	fmt.Printf("NII: %02X %02X\n", bcountryCode[0], bcountryCode[1])

	// 8. Terminal ID: 8 bytes, offset 25
	// 8. running message length: 33 bytes
	// Terminal ID 8 41st field in ISO 8583 message
	// https://www.codeproject.com/Articles/100084/Introduction-to-ISO
	// decimal value: 29110001
	// hex value: 32 39 31 31 30 30 30 31
	trmidOffset := 25
	ipekRequest[trmidOffset+0] = 0x00
	ipekRequest[trmidOffset+1] = 0x00
	ipekRequest[trmidOffset+2] = 0x32
	ipekRequest[trmidOffset+3] = 0x39
	ipekRequest[trmidOffset+4] = 0x31
	ipekRequest[trmidOffset+5] = 0x30
	ipekRequest[trmidOffset+6] = 0x30
	ipekRequest[trmidOffset+7] = 0x31

	// 9. KEY Set ID: 10 bytes, offset 33
	// 9. running message length: 43 bytes
	// Key Set ID 10 bytes 61st field in ISO 8583 message 
	// ksiOffset := 33
	// var ksi = []byte{0xFF, 0xFF, 0x98, 0x76, 0x54, 0x32, 0x10, 0xE0, 0x00, 0x08}
	// copy(ipekRequest[ksiOffset:ksiOffset+10], ksi)
	
	// 10. Data Field: variable, offset 43
	// Data Field Variable 63rd and 62nd field in ISO 8583 message 
	// Method 1 – X.509 Certificate and Encrypted IPEK 
	// Method 2 – TransKrypt’ s Public Key, Encrypted Random Key and 
	// Encrypted IPEK 
	// Method 3 – Terminal’s Public Key and Encrypted IPEK
	// Bitmap will set the following fields 
	// Processing Code – Bit 3 
	// System trace audit number – Bit 11 
	// Network International identifier (NII) – Bit 24 
	// Terminal ID – Bit 41 
	// Response code – Bit 39 
	// Key set IDs - Bit 61 
	// Payload such as Certificates, keys and Encrypted IPEKs – Bit 62 and Bit 63. If 
	// the payload size if greater than 999 it shall span two fields B62 and B63 
	// appropriately tagged. If it fits in one field, field 63 will be used. Also note single 
	// field as well can carry multiple information like exponent and modulus of public 
	// key appropriately tagged. 
	// TRSM ID in IPEK request in each method shall be part of the data fields 63 or 
	// 62. It shall precede the main data of interest in the message. 

	// X.509 PEM Encoded Public Certificate of the terminal with TRSM ID of
	// the terminal as the common name (CN) will be part of the Data field – Field 63.
	// X.509 PEM Encoded Public Certificate of the terminal with TRSM ID of
	// the terminal as the common name (CN) will be part of the Data field – Field 63. 
	// The TRSM ID will be 3 Bytes. With 5 last least significant bits set to ‘0’.
	// The ‘0’ padding shall continue on least significant bits if TRSM is less than 19 bit.
	// The TransKrypt shall validate the public certificate against the TRSM ID which should
	// be the CN field value in the certificate. The response from TransKrypt shall be 
	// encrypted using public key in the certificate. 
	pcertData, err := readPubcertData()
	_ = err
	btrsmID, err := readCommonName(pcertData)
	fmt.Printf("trsmID: ")
	for i:=0; i<4; i++ {
		fmt.Printf("%02X ", btrsmID[i])
	}
	fmt.Println()

	// use this Common Name as TRMS ID in KEY Set ID
	// 9. KEY Set ID: 10 bytes, offset 33
	// 9. running message length: 43 bytes
	// Key Set ID 10 bytes 61st field in ISO 8583 message 
	ksiOffset := 33
	// var ksi = []byte{0xFF, 0xFF, 0x98, 0x76, 0x54, 0x32, 0x10, 0xE0, 0x00, 0x08}
	var ksi = []byte{0xFF, 0xFF, 0x98, 0x76, 0x54, 0x32, 0x10, 0xE0, 0x00, 0x00}
	ksi[5] = btrsmID[0]
	ksi[6] = btrsmID[1]
	ksi[7] = btrsmID[2]
	copy(ipekRequest[ksiOffset:ksiOffset+10], ksi)

	_ = err

	datafieldOffset := 43
	_ = datafieldOffset

	// 10. Data Field: variable, offset 43
	// 10. running message length: 1769 bytes
	// 43 + 2 + 2 + 999 + 2 + 2 + 719 = 1769
	// dataFieldOffset: 43; A1: 2; LEN: 2; FirstPart: 999
	// A1: 2; LEN: 2; SecondPart: 719
	// lenPcertData: 1718 bytes
	// Note: If the public cert exceeds 999 bytes, field 62 shall be used tagging will be 
	// in fields {62} and {63} as follows: 
	// { A1 [len][ First part of public cert]} {A1[len][Second part of public cert]}
	// LLL - 3-digit length indicator (2 bytes BCD)
	pubcertTag := "A1"
	bpubcertTag := []byte(pubcertTag)
	ipekRequest[datafieldOffset+0] = bpubcertTag[0]
	ipekRequest[datafieldOffset+1] = bpubcertTag[1]
	// Field 62, len is 999, LLL in 2 bytes BCD
	ipekRequest[datafieldOffset+2] = 0x09
	ipekRequest[datafieldOffset+3] = 0x99
	lenPcert := len(pcertData)
	lenPcertFirst := 999
	lenPcertSecond := lenPcert - 999
	pcertDataFirst := pcertData[0:999]
	pcertDataSecond := pcertData[999:]
	fmt.Printf("lenPcert: %d\n", lenPcert)
	fmt.Printf("lenPcertFirst: %d\n", lenPcertFirst)
	fmt.Printf("lenPcertSecond: %d\n", lenPcertSecond)

	copy(ipekRequest[datafieldOffset+4:], pcertDataFirst)
	_ = pcertDataSecond

	ipekRequest[datafieldOffset+1003] = bpubcertTag[0]
	ipekRequest[datafieldOffset+1004] = bpubcertTag[1]
	bcdLenSecond := bcd.FromUint32((uint32) (lenPcertSecond))
	fmt.Printf("BCD: %02X %02X\n", bcdLenSecond[2], bcdLenSecond[3])
	ipekRequest[datafieldOffset+1005] = bcdLenSecond[2]
	ipekRequest[datafieldOffset+1006] = bcdLenSecond[3]

	copy(ipekRequest[datafieldOffset+1007:], pcertDataSecond)
	msgLen := (uint16) (datafieldOffset + 1007 + lenPcertSecond)
	bmsgLen := make([]byte, 2)
	binary.BigEndian.PutUint16(bmsgLen, msgLen)

	// 1. Total message length: 2 bytes, offset 0
	fmt.Printf("Total message length: %d\n", msgLen)
	ipekRequest[0] = bmsgLen[0]
	ipekRequest[1] = bmsgLen[1]

	return ipekRequest
	// https://www.techtarget.com/whatis/definition/binary-coded-decimal#:~:text=Binary%2Dcoded%20decimal%20is%20a,numbers%20into%20their%20binary%20equivalents.
	// https://www.codeproject.com/Articles/100084/Introduction-to-ISO
	// BINARY CODED DECIMAL (BCD): LLLL (4-digit BCD)
}

func readPubcertData() ([]byte, error) {
	// https://golang.hotexamples.com/examples/crypto.x509/-/ParseCertificateRequest/golang-parsecertificaterequest-function-examples.html
	csrFile := "./FFFF903128_39898_CERT.pem"
	// l := log.WithField("csr", csrFile)
	// msg := ""
	if !fileExists(csrFile) {
		errMsg := fmt.Sprintf("csr file %q does not exist", csrFile)
		return nil, errors.New(errMsg)
	}

	fmt.Println("csrFile: ", csrFile)
	// l.Debug("read sign request")
	data, err := ioutil.ReadFile(csrFile)
	// fmt.Println("data: ", data)
	fmt.Println("len(data): ", len(data))
	if err != nil {
		return nil, errors.Annotate(err, "read csr file")
	}

	return data, nil
}

func readCommonName(data []byte) ([]byte, error) {
	block, _ := pem.Decode(data)
	if block == nil {
		fmt.Println("Error in decoding PEM file")
		return nil, errors.New("Error in decoding PEM file")
	}

	cert, err := x509.ParseCertificate(block.Bytes)
	_ = err

	fmt.Println("cert.PublicKey: ", cert.PublicKey)
	fmt.Println("cert.PublicKeyAlgorithm: ", cert.PublicKeyAlgorithm)

	fmt.Println("commonName: ", cert.Subject.CommonName)
	fmt.Printf("commonName: %s\n", cert.Subject.CommonName)
	commonName := cert.Subject.CommonName
	trsmID, err := strconv.Atoi(commonName)
	fmt.Println("trsmID: ", trsmID)
	fmt.Printf("trsmID: %08X\n", trsmID)
	btrsmID := make([]byte, 4)
	binary.BigEndian.PutUint32(btrsmID, (uint32) (trsmID))
	// trsmID = trsmID << 2;
	// fmt.Println("trsmID: \n", trsmID)

	for i:=0; i<4; i++ {
		fmt.Printf("%02X ", btrsmID[i])
	}
	fmt.Println()

	trsmID = trsmID << 5;
	binary.BigEndian.PutUint32(btrsmID, (uint32) (trsmID))
	for i:=0; i<4; i++ {
		fmt.Printf("%02X ", btrsmID[i])
	}
	fmt.Println()

	// 3 bytes shifted left 5 bits
	// TRSM ID: 19 bits consisting of 8 bits, 8 bits, 3 bits

	return btrsmID, err
}

func fileExists(path string) bool {
    _, err := os.Stat(path)
    return !errors.Is(err, os.ErrNotExist)
}

func parseIpekResponse(ipekResponse []byte) error {
	// IPEK Response: 
	// The response from AG to terminal shall carry IPEK encrypted with the public key 
	// that was shared by the terminal in the IPEK REQ message 
	// Field 63 will have message type as “A2” for IPEK-RESP 
	// The SA byte in the TPDU header will be 0x00 x02 
	// ISO 8583 Enabled Terminal:

	// 1. Total message length: 2 bytes, offset 0
	// 2. TPDU Header: 5 bytes, offset 2
	// 3. MTI: 2 bytes, offset 7
	// 4. Bit map: 8 bytes, offset 9
	// 5. Processing code: Field 3, 3 bytes, offset 17
	// 6. System Trace Audit Number: 3 bytes, offset 20
	// 7. NII: 2 bytes, offset 23
	// 8. Response code: 2 bytes, offset 25
	// 9. Terminal ID: 8 bytes, offset 27
	// 10. Data Field: variable, offset 35

	// 1. Total message length: 2 bytes, offset 0
	msgLen := binary.BigEndian.Uint16(ipekResponse[0:2])
	fmt.Println("parseIpekResponse()")
	fmt.Printf("Total message length of IPEK Response: %d\n", msgLen)

	// 2. running message length: 7 bytes
	// TPDU Header 5 Byte 1 = 0x60 
	// Byte 2&3 = NII (can be 0x00 for EIS based messages) 
	// Byte 4&5 
	// Method 1: 
	// IPEK REQ [0x00,0x01] 
	// IPEK RESP [0x00,0x02] 
	// Method 2: 
	// PUB_KEY REQ [0x00,0x03] 
	// PUB_KEY RESP [0x00,0x04] 
	// IPEK REQ [0x00,0x05] 
	// IPEK RESP [0x00,0x06] 
	// Method 3: 
	// IPEK REQ [0x00,0x07] 
	// IPEK RESP [0x00,0x08] 

	tpduOffset := 2
	fmt.Printf("TPDU Header: ")
	for i:= 0; i<5; i++ {
		fmt.Printf("%02X ", ipekResponse[tpduOffset+i])
	}
	fmt.Printf("\n")

	// simulation
	ipekResponse[tpduOffset+3] = 0x00 
	ipekResponse[tpduOffset+4] = 0x02

	// The SA byte in the TPDU header will be 0x00 x02; byte 4 and byte 5
	if ( ipekResponse[tpduOffset+3] != 0x00 || 
		 ipekResponse[tpduOffset+4] != 0x02 ) {
		fmt.Println("Error: The SA byte in the TPDU header must be 0x00 x02");
		// return errors.NewErr("error")
		return nil
	} 

	// 3. MTI: 2 bytes, offset 7
	mtiOffset := 7
	fmt.Printf("MTI: %02X %02X\n", ipekResponse[mtiOffset+0], ipekResponse[mtiOffset+0])

	// 4. Bit map: 8 bytes, offset 9
	bitmapOffset := 9
	_ = bitmapOffset
	// 1 2 ...								32
	// 0010 0000 0010 0000 0000 0001 0000 0000
	// 33 34 ...							64
	// 0000 0000 1000 0000 0000 0000 0000 1110
	// hexadecimal: 0x20 0x20 0x01 0x00
	// hexadecimal: 0x00 0x80 0x00 0x08
	// ipekResponse[bitmapOffset+0] = 0x20
	// ipekRequest[bitmapOffset+1] = 0x20
	// ipekRequest[bitmapOffset+2] = 0x01
	// ipekRequest[bitmapOffset+3] = 0x00
	// ipekRequest[bitmapOffset+4] = 0x00
	// ipekRequest[bitmapOffset+5] = 0x80
	// ipekRequest[bitmapOffset+6] = 0x00
	// ipekRequest[bitmapOffset+7] = 0xE0

	// 4. Bit map: 8 bytes, offset 9
	// 4. running message length: 17 bytes
	// Bits will be set for fields present in the ISO 8583 message
	// 1 2 ...								32
	// 0010 0000 0010 0000 0000 0001 0000 0000
	// 33 34 ...							64
	// 0000 0000 1000 0000 0000 0000 0000 1110
	// hexadecimal: 0x20 0x20 0x01 0x00
	// hexadecimal: 0x00 0x80 0x00 0x08

	// Bit map: Processing code Field 3, STAN Field 11, NII Field 24,
	// Bit map: Response Code Field 39, Terminal ID Field 41
	// Bit map: Field 61, Encrypted IPEK Field 63
	// the length of public certificate data is greater than 999
	// Field 62 is set, Field 63 is set
	// Processing Code 3, 3rd field in ISO 8583 message 
	// Following values are encoded for each method in request and response 
	// Method 1 = [0x31,0x00,0x00] 
	// Method 2 = [0x32,0x00,0x00] 
	// Method 3 = [0x33,0x00,0x00]
	bitMask3 :=  []byte{0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
	bitMask11 := []byte{0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00}
	bitMask24 := []byte{0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00}
	bitMask39 := []byte{0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00}
	bitMask41 := []byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00}
	bitMask63 := []byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02}
	
	res := (byte) (0)
	res = ipekResponse[bitmapOffset+0] & bitMask3[0]
	if ( res != 0 ) {
		fmt.Println("Processing code Field 3 is present.")
	} else {
		fmt.Println("Error: Processing code Field 3 is NOT present.")
	}
	res = ipekResponse[bitmapOffset+2] & bitMask11[2]
	if ( res != 0 ) {
		fmt.Println("STAN Field 11 is present.")
	} else {
		fmt.Println("Error: STAN Field 11 is NOT present.")
	}
	res = ipekResponse[bitmapOffset+2] & bitMask24[2]
	if ( res != 0 ) {
		fmt.Println("NII Field 24 is present.")
	} else {
		fmt.Println("Error: NII Field 24 is NOT present.")
	}
	res = ipekResponse[bitmapOffset+4] & bitMask39[4]
	if ( res != 0 ) {
		fmt.Println("Response code Field 39 is present.")
	} else {
		fmt.Println("Error: Response code Field 39 is NOT present.")
	}
	res = ipekResponse[bitmapOffset+5] & bitMask41[5]
	if ( res != 0 ) {
		fmt.Println("Terminal ID Field 41 is present.")
	} else {
		fmt.Println("Error: Terminal ID Field 41 is NOT present.")
	}
	res = ipekResponse[bitmapOffset+7] & bitMask63[7]
	if ( res != 0 ) {
		fmt.Println("Encrypted IPEK Field 63 is present.")
	} else {
		fmt.Println("Error: Encrypted IPEK Field 63 is NOT present.")
	}
	// fmt.Println("initipek.go: line 567")
	// os.Exit(0)
	// Bit map: Response Code Field 39, Terminal ID Field 41
	// Bit map: Field 61, Encrypted IPEK Field 63


	// 5. Processing code: Field 3, 3 bytes, offset 17
	// 5. Processing code: Field 3, 3 bytes, offset 17
	// 5. running message length: 20 bytes
	// 3rd field in ISO 8583 message 
	// Following values are encoded for each method in request and response 
	// Method 1 = [0x31,0x00,0x00] 
	// Method 2 = [0x32,0x00,0x00] 
	// Method 3 = [0x33,0x00,0x00] 
	
	// Method 1 = [0x31,0x00,0x00] 
	procOffset := 17
	fmt.Printf("Processing code: %02X %02X %02X\n", ipekResponse[procOffset+0],
			ipekResponse[procOffset+1], ipekResponse[procOffset+2])

	// 6. System Trace Audit Number: 3 bytes, offset 20
	auditOffset := 20
	// var stanNumber uint32 = 257
	// bstanNumber := make([]byte, 4)
	fmt.Printf("STAN: %02X %02X %02X\n", ipekResponse[auditOffset+0],
			ipekResponse[auditOffset+1], ipekResponse[auditOffset+2])

	// 7. NII: 2 bytes, offset 23
	niiOffset := 23
	fmt.Printf("NII: %02X %02X\n", 
			ipekResponse[niiOffset+1], ipekResponse[niiOffset+2])

	// 8. Response code: 2 bytes, offset 25
	respOffset := 25
	fmt.Printf("Response code: %02X %02X\n", 
			ipekResponse[respOffset+1], ipekResponse[respOffset+2])
	// 9. Terminal ID: 8 bytes, offset 27
	termOffset := 25
	fmt.Printf("Terminal ID: ");
	for i:=0; i<8; i++ {
		fmt.Printf("%02X ", ipekResponse[termOffset+i]);
	}
	fmt.Printf("\n")
	// Field 63 will have message type as “A2” for IPEK-RESP
	
	// 10. Data Field: variable, offset 35
	// IPEK Response: 
	// {A2 [LEN] [ENCRYPTED IPEK]}
	ipekOffset := 35 
	ipekrespTag := "A2"
	bipekrespTag := []byte(ipekrespTag)
	if ( ipekResponse[ipekOffset+0] == bipekrespTag[0] ) {
		fmt.Println("ipekTag[0] is A.")
	} else {
		fmt.Println("ipekTag[0] is NOT A.")
	}
	if ( ipekResponse[ipekOffset+1] == bipekrespTag[1] ) {
		fmt.Println("ipekTag[1] is 2.")
	} else {
		fmt.Println("ipekTag[1] is NOT 2.")
	}

	bLen := make([]byte, 4)
	copy(bLen[1:4], ipekResponse[ipekOffset+2:ipekOffset+2+3])
	for i:=0; i<3; i++ {
		fmt.Printf("LEN: %02x %02X\n", bLen[1+i], ipekResponse[ipekOffset+2+i])
	}

	bcdLenIPEK := bcd.ToUint32(bLen)
	fmt.Printf("BCD LEN: %d\n", bcdLenIPEK)

	// 1-2 IPEK Req -- IPEK Request is generated by POS and sent to AG.
	// IPEK Request consists of Key Set ID and the public certificate of 
	// the terminal. The public certificate shall have TRSM ID as its CN.
	// The key length can be a configurable parameter. AG on reception of
	// IPEK Req will transparently pass it to TransKrypt Server.
	// 
	// 3-4 IPEK Resp -- TransKrypt will validate the terminal against the
	// certificate sent in the IPEK REQ. It shall also validate the certificate
	// against the stored Certificate Authority (CAu) certificates. It will 
	// then generate IPEK using KSN, TRSM ID and BDK and encrypt IPEK with 
	// the terminal’s public certificate. AG will transparently forward it
	// to POS/TMS. 

	// to be continued: 2023-01-18
	// read public key
	pcertData, err := readPubcertData()
	_ = err

	block, _ := pem.Decode(pcertData)
	if block == nil {
		fmt.Println("Error in decoding PEM file")
		return errors.New("Error in decoding PEM file")
	}

	cert, err := x509.ParseCertificate(block.Bytes)
	_ = err

	fmt.Println("cert.PublicKey: ", cert.PublicKey)
	fmt.Println("cert.PublicKeyAlgorithm: ", cert.PublicKeyAlgorithm)
	// os.Exit(0)

	var testIpek = []byte{0x6a, 0xc2, 0x92, 0xfa, 0xa1, 0x31, 0x5b, 0x4d,
		0x85, 0x8a, 0xb3, 0xa3, 0xd7, 0xd5, 0x93, 0x3a}

	// publicKey := cert.PublicKey
	// publicKey := cert.PublicKey
	publicKey := cert.PublicKey.(*rsa.PublicKey)
	ciphertext := EncryptWithPublicKey(testIpek, publicKey)
	size := len(ciphertext)
	for i:=0; i<size; i++ {
		fmt.Printf("%02X ", ciphertext[i])
	}
	fmt.Println()
	// os.Exit(0)

	privateKeyPath := "./FFFF903128_39898_PRIV.pem"
	// encryptedMessage := string(ciphertext)
	plaintext, err := Decrypt(privateKeyPath, ciphertext)
	_ = err

	fmt.Printf("testIpek: ")
	size = len(testIpek)
	for i:=0; i<size; i++ {
		fmt.Printf("%02X ", testIpek[i])
	}

	fmt.Println()
	fmt.Printf("plaintext: ")
	size = len(plaintext)
	for i:=0; i<size; i++ {
		fmt.Printf("%02X ", plaintext[i])
	}
	fmt.Println()
	// os.Exit(0)

	// plaintext := DecryptWithPrivateKey(ciphertext, )
	// https://golangdocs.com/rsa-encryption-decryption-in-golang
	// https://gist.github.com/miguelmota/3ea9286bd1d3c2a985b67cac4ba2130a
	// https://www.sohamkamani.com/golang/rsa-encryption/

	// encrypt IPEK with public key
	// read private key
	// decrypt IPEK with private key
	//
	
	return nil
}

// EncryptWithPublicKey encrypts data with public key
func EncryptWithPublicKey(msg []byte, pub *rsa.PublicKey) []byte {
	hash := sha512.New()
	ciphertext, err := rsa.EncryptOAEP(hash, rand.Reader, pub, msg, nil)
	if err != nil {
		// log.Error(err)
		fmt.Println("ERROR: ", err)
	}
	return ciphertext
}

// DecryptWithPrivateKey decrypts data with private key
func DecryptWithPrivateKey(ciphertext []byte, priv *rsa.PrivateKey) []byte {
	hash := sha512.New()
	plaintext, err := rsa.DecryptOAEP(hash, rand.Reader, priv, ciphertext, nil)
	if err != nil {
		// log.Error(err)
		fmt.Println("ERROR: ", err)
	}
	return plaintext
}

// http://www.inanzzz.com/index.php/post/1nw3/data-encryption-and-decryption-with-x509-public-and-private-key-example-in-golang#
func Decrypt(privateKeyPath string, encryptedMessage []byte) ([]byte, error) {
	privBytes, err := ioutil.ReadFile(privateKeyPath)
	fmt.Println("Private key file: ", privateKeyPath)
	// os.Exit(0)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(privBytes)
	if block == nil {
		fmt.Println("Error in decoding PEM file")
		// var str1 string = ""
		return nil, errors.New("Error in decoding PEM file")
	}

	priv, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	_ = err
	privkey := priv.(*rsa.PrivateKey)
	hash := sha512.New()
	// 	ciphertext, err := rsa.EncryptOAEP(hash, rand.Reader, pub, msg, nil)
	plainMessage, err := rsa.DecryptOAEP(hash, rand.Reader, privkey, encryptedMessage, nil)
	// fmt.Println("plaintext: ", string(plainMessage))
 
	return plainMessage, err
}

func convertBytesToPrivateKey(keyBytes []byte) (*rsa.PrivateKey, error) {
	var err error
 
	block, _ := pem.Decode(keyBytes)
	blockBytes := block.Bytes
	ok := x509.IsEncryptedPEMBlock(block)
 
	if ok {
		blockBytes, err = x509.DecryptPEMBlock(block, nil)
		if err != nil {
			return nil, err
		}
	}
 
	privateKey, err := x509.ParsePKCS1PrivateKey(blockBytes)
	if err != nil {
		return nil, err
	}
 
	return privateKey, nil
}
 
func pemStringToCipher(encryptedMessage string) []byte {
	b, _ := pem.Decode([]byte(encryptedMessage))
 
	return b.Bytes
}
